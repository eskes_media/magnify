# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'magnifier/version'

Gem::Specification.new do |spec|
  spec.name          = "magnifier"
  spec.version       = Magnifier::VERSION
  spec.authors       = ["Henk Meijer", "Kobus Post"]
  spec.email         = ["meijerhenk@gmail.com"]
  spec.description   = %q{Communicate with the Eskes Media Magazine Maker}
  spec.summary       = %q{This gem makes it realy easy to communicate to the Magazine Maker in a ruby-esque way.}
  spec.homepage      = "https://bitbucket.org/eskes_media/magnify"
  spec.license       = "MIT"

  spec.post_install_message = "Thanks for installing! Please run 'rails generate magnifier' to install the options file."

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
