class MagnifierGenerator < Rails::Generators::Base

  source_root File.join(File.dirname(__FILE__), "templates")

  # Description:
  # Copies the migration file into the application.  
  def install
    # Example:
    #     rails generate magnifier

    copy_options_file
  end

  private


  
  def copy_options_file
    copy_file File.join(config_path, 'magnifier_options.yml'), config_destination
  end

  def config_path
    File.join(%w(.. .. .. config))
  end
  
  def config_destination
    'config/magnifier_options.yml'
  end

end
