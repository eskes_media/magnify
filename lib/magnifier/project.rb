include Magnifier
class Project < ActiveResource::Base
	headers['X-AUTH-TOKEN'] = Settings.api_key
  self.site = Settings.site

  # make a copy of the project
  def duplicate
  	new_project = self.get :pick_template
  	npid = new_project["id"]
  	return Project.find(npid)
  end

  def pages(scope = :all)
    Page.find(scope, :params => {:project_id => self.id})
  end

  def make_previews
    self.get :make_previews
  end

end
