include Magnifier

class Page < ActiveResource::Base
	headers['X-AUTH-TOKEN'] = Settings.api_key
  self.site = Settings.site

  # forces the magazin make engine to produce a new preview of this page
  def make_preview
  	self.get :make_preview
  end
end
