include Magnifier
class PageElement < ActiveResource::Base
	headers['X-AUTH-TOKEN'] = Settings.api_key
  self.site = Settings.site

  def replace_content(old_value, new_value)
  	if content.include? old_value
	  	c = content.gsub(old_value, new_value)
	  	self.content = c
	  	self.save
	  else
	  	return false
	  end
  end

  # upload or use a photo
  def use_photo(url)
    self.post :update_with_photo, url: url
  end
end
