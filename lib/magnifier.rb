require "magnifier/version"

require 'magnifier/railtie' if defined?(Rails)

module Magnifier
	class Settings

		def self.options
			YAML::load(File.open("config/magnifier_options.yml"))
		end

		def self.site
			begin
				options['magnifier_options'][Rails.env]['site']
			rescue

			end
		end

		def self.api_key
			begin
			options['magnifier_options'][Rails.env]['api_key']
			rescue

			end
		end
	end

	require "magnifier/project"
	require "magnifier/page"
	require "magnifier/page_element"


	# private

end
