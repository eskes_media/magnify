# Magnifier

Magnifier makes it possible to communicate with the Eskes Media Magazine Maker engine and perform various tasks (duplicate, make_preview, etc.) to projects, without having to deal with the API directly. To communicate with the API, you need an API-key, provided by the Magazine Maker.

## Installation

Add this line to your application's Gemfile:

    gem 'magnifier'

And then execute:

    $ bundle

    # after installation, run this command to install the settings file (.yml)
    $ rails generate magnifier

Or install it yourself as:

    $ gem install magnifier

## Usage

### Project
- To fetch a list of all projects: `Magnifier::Project.all`
- To fetch a project: `Magnifier::Project.find(123)`

#### Public methods
	# duplicate
	# duplicates the current project. Example:
	new_project = Project.find(123).duplicate

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
